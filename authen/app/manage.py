from main import create_app
import os

app_server = create_app(os.getenv("DEPLOY_ENV") or "development")


@app_server.before_first_request
def initial():
    from main.initialize import initialize


if __name__ == '__main__':
    try:
        app_server.run(host='0.0.0.0', port=5555)
    except Exception as e:
        print(e)
        pass
