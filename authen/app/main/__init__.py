
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

import mysql.connector
from click import echo
from mysql.connector import Error
import time
from main.common.configs.app_config import config_by_name, DatabaseInfo


flag = True
while flag:
    try:
        mydb = mysql.connector.connect(
            host=DatabaseInfo.MYSQL_HOST,
            user=DatabaseInfo.MYSQL_USERNAME,
            password=DatabaseInfo.MYSQL_PASS
        )
        mycursor = mydb.cursor()
        mycursor.execute("CREATE SCHEMA IF NOT EXISTS " +
                         DatabaseInfo.DEFAULT_SCHEMA + " DEFAULT CHARACTER SET utf8")
        mycursor.close()
        mydb.close()
        flag = False
    except Error as e:
        print(e)
        time.sleep(1)
        print("Retry to connect to mysql ...")

db = SQLAlchemy()


def create_app(config_type='development'):
    app = Flask(__name__)
    CORS(app)
    app.config.from_object(config_by_name[config_type])

    initialize_extensions(app)
    register_blueprints(app)
    register_cli_commands(app)

    return app


def initialize_extensions(app: Flask):
    db.init_app(app)


def register_blueprints(app: Flask):
    from main.controllers.user_controller import user_blueprint
    app.register_blueprint(user_blueprint)


def register_cli_commands(app):
    @app.cli.command('init_db')
    def initialize_database():
        """Initialize the database."""
        db.drop_all()
        db.create_all()
        echo('Initialized the database!')
