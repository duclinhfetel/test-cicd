from main import db
import uuid


class User(db.Model):
    __tablename__ = 'users'
    uid = db.Column(db.String(32), primary_key=True, unique=True,
                    nullable=False, default=lambda: uuid.uuid4().hex)
    name = db.Column(db.String(50))
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.Text)

    def serialize(self):
        return {
            "uid": self.uid,
            "name": self.name,
            "email": self.email,
            "password": self.password
        }
    
    def __repr__(self) -> str:
        return self.email

    def add(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()
