from os import environ
import urllib.parse as parser
import json

class DatabaseInfo:
    """ Config database"""
    MYSQL_HOST = environ.get('MYSQL_HOST')
    MYSQL_USERNAME = environ.get('MYSQL_USERNAME')
    MYSQL_PASS = environ.get('MYSQL_PASS')
    MYSQL_PASS_QUOTE = parser.quote(MYSQL_PASS)

    DEFAULT_SCHEMA = "iam"

    BASE_URI_MYSQL = "mysql+mysqlconnector://" + \
        MYSQL_USERNAME + ":" + MYSQL_PASS_QUOTE + "@" + \
        MYSQL_HOST + "/"


class Config(DatabaseInfo):
    SECRET_KEY = environ.get('SECRET_KEY')
    DEBUG = False

    # database
    SQLALCHEMY_DATABASE_URI = DatabaseInfo.BASE_URI_MYSQL + \
        DatabaseInfo.DEFAULT_SCHEMA + "?charset=utf8"

    # SQLALCHEMY_BINDS = {
    #     'social': DatabaseInfo.BASE_URI_MYSQL + DatabaseInfo.SOCIAL_SCHEMA + "?charset=utf8"
    # }

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_POOL_RECYCLE = 3600 


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = False


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    DEBUG = True


config_by_name = dict(
    development=DevelopmentConfig,
    test=TestingConfig,
    production=ProductionConfig
)

key = Config.SECRET_KEY
