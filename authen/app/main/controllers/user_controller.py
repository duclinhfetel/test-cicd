import json
from flask import request, Blueprint
from main.models.user import User

user_blueprint = Blueprint("user_blueprint", __name__)


@user_blueprint.route("/login", methods=['POST'])
def login():
    if request.form:
        form = request.form
    elif request.data:
        form = json.loads(request.data)
    else:
        return "fail", 400
    if not 'email' in form or not 'password' in form:
        return "missing field", 400

    email = form['email']
    password = form['password']

    user = User.query.filter(User.email == email).first()
    if not user:
        return form, 400

    if password != user.password:
        return "password incorrect", 400

    return "login success"


@user_blueprint.route("/register", methods=['POST'])
def register():
    if request.form:
        form = request.form
    elif request.data:
        form = json.loads(request.data)

    email = form['email']
    password = form['password']

    if User.query.filter(User.email == email).first():
        return "existed user", 400

    user = User(name="Test", email=email, password=password)
    user.add()
    return "add ok"


@user_blueprint.route("/logout", methods=['GET'])
def logout():
    return "ok"
