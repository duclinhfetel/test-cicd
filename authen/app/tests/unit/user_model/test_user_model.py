from curses import use_default_colors
from main.models.user import User


def test_new_user():
    user = User(email='linh3@gmail.com', password='Metadoc')
    assert user.email == 'linh3@gmail.com'
    assert user.password == 'Metadoc'
    # assert user.__repr__() == 'linh3@gmail.com'


def test_new_user_with_fixture(new_user):
    assert new_user.email == 'linh@gmail.com'
    assert new_user.password == 'Metadoc'
