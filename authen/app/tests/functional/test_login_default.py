
def test_get_login_page(test_client):
    response = test_client.get('/login')
    assert response.status_code == 405
    # assert b'login' in response.data


def test_valid_login(test_client):
    response = test_client.post('/login',
                                data={'email': 'linh@gmail.com', 'password': 'Metadoc'})
    print(response.data)
    assert response.status_code == 200
    # assert b'login success' not in response.data
