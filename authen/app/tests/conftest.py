import pytest

from main import db, create_app
from main.models.user import User

# Fixtures


@pytest.fixture(scope='module')
def new_user():
    user = User(email='linh@gmail.com', password='Metadoc')
    return user


# @pytest.fixture(scope='module')
def init_database():
    db.create_all()
    user1 = User(email='linh@gmail.com', password='Metadoc')
    user2 = User(email='linh1@gmail.com', password='Metadoc')
    db.session.add(user1)
    db.session.add(user2)
    db.session.commit()


@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app('test')
    with flask_app.test_client() as testing_client:
        with flask_app.app_context():
            db.create_all()
            user1 = User(email='linh@gmail.com', password='Metadoc')
            user2 = User(email='linh1@gmail.com', password='Metadoc')
            db.session.add_all([user1, user2]) 
            db.session.commit()
            yield testing_client





@pytest.fixture(scope='module')
def cli_test_client():
    flask_app = create_app('test')
    # flask_app.config.from_object('config.TestingConfig')

    runner = flask_app.test_cli_runner()

    yield runner  # this is where the testing happens!
